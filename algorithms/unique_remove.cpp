#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

int main() {
    std::vector<int> consecutive_repeats{ 7, 6, 6, 1, 8, 9, 9 };
    std::vector<int> mixed_repeats{ 9, 7, 6, 1, 6, 8, 9 };

    /* remove consecutive duplicated elements */
    {
    auto first_non_unique{std::unique(consecutive_repeats.begin(), consecutive_repeats.end())}; // moves unwanted to the end
    consecutive_repeats.erase(first_non_unique, consecutive_repeats.end());                     // erases unwanted from the end
    std::copy(begin(consecutive_repeats), end(consecutive_repeats),
              std::ostream_iterator<int>(std::cout, " "));
    }

    std::cout << std::endl;

    /* remove non-consecutive (mixed) duplicated elements */
    {
    std::sort(mixed_repeats.begin(), mixed_repeats.end());                                      // sort before removing duplicates
    auto first_non_unique{std::unique(mixed_repeats.begin(), mixed_repeats.end())};             // moves unwanted to the end
    mixed_repeats.erase(first_non_unique, mixed_repeats.end());                                 // erases unwanted from the end
    std::copy(begin(mixed_repeats), end(mixed_repeats),
              std::ostream_iterator<int>(std::cout, " "));
    }

    return 0;
}
