#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector v{ 1, 2, 3, 4, 5 };
    std::vector z{ 5, 8, 2 };
    
    auto it = std::find_if(begin(v), end(v),
                           [](auto e) {
                               return e >= 3;
                           });
    if (it != v.end()) {                                // if found
        std::cout << *it << std::endl;
    }

    /* searches the FIRST range for any of the elements in the SECOND range */
    it = std::find_first_of(begin(v), end(v),
                            begin(z), end(z));          // is 1 of v in z?
                                                        // is 2 of v in z?  IS
    if (it != v.end()) {
        std::cout << *it << std::endl;
    }

    return 0;
}
