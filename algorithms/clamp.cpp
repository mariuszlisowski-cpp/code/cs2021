#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    const int LOW{ 20 };                                    // clamp range
    const int HI{ 80 };                                     // [20, 80]

    std::cout << std::clamp(50, LOW, HI) << '\n';           // 50 returned (value)
    std::cout << std::clamp(10, LOW, HI) << '\n';           // LOW returned (value lower)
    std::cout << std::clamp(90, LOW, HI) << '\n';           // HI returned (value higher)

    for (auto v : { 10, 200, -1}) {
        std::cout << std::clamp(v, 0, 100) << ' ';          // 10 passed only (rest clamped)
    }

    return 0;
}
