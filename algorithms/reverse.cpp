#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    std::reverse(begin(v), end(v));
    print_container(v);

    return 0;
}
