#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(container.begin(), container.end(),
              std::ostream_iterator<typename T::value_type>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 5, 6, 4, 3, 2, 6, 7, 9, 3 };
    std::nth_element(begin(v),                                      // begin
                     begin(v) + v.size() / 2,                       // mid
                     end(v));                                       // end
    /* { 3, 5, 3, 4, 5, 6, 7, 9, 6 }                                // median
                   |mid|                                            // mid is 5 value
      less or equal      bigger                                     // not sorted (not need to sort)
     */
    print(v);

    return 0;
}
