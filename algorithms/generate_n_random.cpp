#include <algorithm>
#include <functional>               // std::ref
#include <iostream>                 // std::cout
#include <iterator>                 // std::ostream_iterator
#include <random>                   // std::mt19937

int main() {
    std::mt19937 mt;

    std::generate_n(std::ostream_iterator<std::mt19937::result_type>(std::cout, " "),
                    10,
                    std::ref(mt));

    return 0;
}
