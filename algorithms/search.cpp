#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector v{ 1, 2, 3, 4, 5 };
    std::vector z{ 3, 4, 5 };

    /* searches for the first occurrence of SECOND range in the FIRST range */
    auto it = std::search(begin(v), end(v),                 // FIRST range
                          begin(z), end(z));                // SECOND range
                                                            // is z {3, 4, 5} in v? IS
    if (it != v.end()) {
        std::cout << *it << std::endl;                      // beginning of first occurence in FIRST
    }

    return 0;
}
