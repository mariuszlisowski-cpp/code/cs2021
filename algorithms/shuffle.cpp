#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    std::shuffle(begin(v), end(v),
                 std::mt19937(std::random_device{}()));

    std::copy(begin(v), end(v),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
