#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

template<template <typename, typename> class Container,
         typename T,
         typename Allocator = std::allocator<T> >
void print_container(const Container<T, Allocator>& container) {
    // truncated output of ten elements only (temp magic value)
    std::copy(begin(container), begin(container) + 10,  
              std::ostream_iterator<T>(std::cout, " "));
    std::cout << "..." << std::endl;
}

int main() {
    /* 1. Create a vector with values [1, 1000] */
    std::vector<int> v1(1000);
    std::iota(begin(v1), end(v1), 1);
    std::cout << "1. ";
    print_container(v1);

    /* 2. Count the sum of all elements in a vector */
    auto sum = std::accumulate(begin(v1), end(v1), 0);
    std::cout << "2: " << sum << std::endl;

    /* 3. Create a vector and fill it sequentially */
    std::vector<int> v2(1000);
    std::generate(v2.begin(), v2.end(),
                  [i{ 0 }, b{ false }]() mutable {
                      if (i % 2) b = !b;
                      return i++ % 2
                             ? 0
                             : (b ? 1 : -1);
                  });
    std::cout << "3. ";
    print_container(v2);

    /* 4. Count the product of all elements in vectors */
    auto product = std::inner_product(begin(v1), end(v1),
                                      begin(v2),
                                      0);
    std::cout << "4: " << product << std::endl;

    /* 5. Count the sum of v1 elements but only from
          positions where there are positive ones in v2 */
    auto result = std::inner_product(begin(v1), end(v1),
                                     begin(v2),
                                     0,
                                     std::plus<>(),
                                     [](const auto lhs, const auto rhs) {
                                         return rhs == 1 ? lhs : 0;
                                     });
    std::cout << "5: " << result << std::endl;
    /* v1{  1, 2, 3, 4,  5, 6, 7, 8,  9, 10, 11 ...}
       v2{ -1, 0, 1, 0, -1, 0, 1, 0, -1, 0,  1  ...}
         {        3            7             11 ...} */

    return 0;
}
