#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    /* peforms a left rotation */
    std::rotate(begin(v), begin(v) + 2 , end(v));       // middle becomes the first
    print_container(v);

    return 0;
}
