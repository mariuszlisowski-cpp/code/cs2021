#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(container.begin(), container.end(),
              std::ostream_iterator<typename T::value_type>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 5, 6, 4, 3, 2, 6, 7, 9, 3 };
    /* pick elements tp sort from the whole range */
    std::partial_sort(begin(v),                                     // begin
                      begin(v) + 3,                                 // mid (exclusive)
                      end(v));                                      // end
    /* { 2, 3, 3, 6, 5, 6, 7, 9, 4 }
         0  1  2  3                                                 // 1-2 indices sorted
        |sorted| |   unspecified   |                                // O(n*log(n)
     */
    print(v);

    return 0;
}
