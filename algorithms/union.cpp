#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(begin(container), end(container),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> sum_of_containers;

    std::vector<int> v1{ 1, 2, 3, 4, 5 }; 
    std::vector<int> v2{       3, 4, 5, 6, 7 }; 
    //                 { 1, 2, 3, 4, 5, 6, 7 }                      // union
    std::set_union(v1.begin(), v1.end(),
                   v2.begin(), v2.end(),
                   std::back_inserter(sum_of_containers));
    print(sum_of_containers);

    std::vector<int> v3{ 1, 2, 5, 5, 5, 9 };
    std::vector<int> v4{    2, 5, 7 };
    //                 { 1, 2, 5, 5, 5, 9 };                        // union
    std::set_union(v3.begin(), v3.end(),
                   v4.begin(), v4.end(),
                   sum_of_containers.begin());                      // caution: overwrites
    print(sum_of_containers);

    return 0;
}
