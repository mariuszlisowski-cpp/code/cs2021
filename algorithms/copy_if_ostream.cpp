#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 8, 2, 5, 3, 4, 4, 2, 7, 6, 6, 1, 8, 9 };

    /* find all equal to 2, 4, 6 or 8 */
    std::copy_if(begin(v), end(v),
                 std::ostream_iterator<int>(std::cout, " "),
                 [](auto el) {
                     return el == 2 || el == 4 || el == 6 || el == 8;
                 });
    
    std::cout << '\n';

    /* find all greater than six */
    std::copy_if(begin(v), end(v),
                 std::ostream_iterator<int>(std::cout, " "),
                 [](auto el) {
                     return el > 6;
                 });

    return 0;
}
