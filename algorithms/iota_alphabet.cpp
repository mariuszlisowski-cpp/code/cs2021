#include <iostream>
#include <iterator>
#include <numeric>
#include <list>

template<template <typename, typename> class Container, 
         typename T,
         typename Allocator = std::allocator<T> >
void print(const Container<T, Allocator>& constainer) {
    std::copy(begin(constainer), end(constainer),
              std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    const size_t ALPHABET_SIZE{ 26 };
    std::list<char> l(ALPHABET_SIZE);
    
    std::iota(l.begin(), l.end(), 'a');

    print(l);

}
