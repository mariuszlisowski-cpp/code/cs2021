#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(begin(container), end(container),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    auto is_even = [](auto i) { return i % 2; };                                // check for even number

    std::vector v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::stable_partition(v1.begin(), v1.end(), is_even);                       // preserves relative order
    print(v1);
    // { 1, 3, 5, 7, 9, 2, 4, 6, 8 }
    //      odd       |    even

    std::vector v2 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::partition(v2.begin(), v2.end(), is_even);                              // random order of partitions
    print(v2);
    // { 1, 9, 3, 7, 5, 6, 4, 8, 2 }
    //      odd       |    even

    return 0;
}
