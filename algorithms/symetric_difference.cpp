#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(begin(container), end(container),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> common_part;
    
    std::vector<int> v1{ 1, 2, 3, 4, 5 }; 
    std::vector<int> v2{       3, 4, 5, 6, 7 }; 
    //                 { 1, 2           6, 7 };                         // symetric difference
    std::set_symmetric_difference(v1.begin(), v1.end(),
                                  v2.begin(), v2.end(),
                                  std::back_inserter(common_part));
    print(common_part);

    std::vector<int> v3{ 1, 2, 5, 5, 5, 9, 3 };
    std::vector<int> v4{    2, 5,       9, 7 };
    //                 { 1,       5, 5,    3};                          // symetric difference
    std::set_symmetric_difference(v3.begin(), v3.end(),
                                  v4.begin(), v4.end(),
                                  common_part.begin());                 // caution: overwrites
    print(common_part);

    return 0;
}
