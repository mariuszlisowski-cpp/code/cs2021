#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::cout << std::boolalpha;
    std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9};                           // not partitioned

    auto is_even = [](auto i) { return i % 2; };                                // check for even number

    /* partition */
    auto it = std::partition(v.begin(), v.end(), is_even);
    std::cout << std::is_partitioned(v.begin(), v.end(), is_even);              // true (partitioned)

    /* get iterator only */
    auto pp = std::partition_point(v.begin(), v.end(), is_even);

    // { 8, 2, 6, 4, 5, 3, 7, 1, 9 };
    //    even      |      odd
    //              it
    //              pp

    return 0;
}
