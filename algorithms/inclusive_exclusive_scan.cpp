#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

int main() {
    std::vector<int>v(10, 2);                                           // {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}

    /* regard current element */
    std::inclusive_scan(begin(v),                                       // same as std::partial_sum
                        end(v),
                        std::ostream_iterator<int>(std::cout, " "),
                        std::multiplies<int>());                        // binary predicate

    /* {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
        2   
           4                                                            // 2 * 2
              8                                                         // 2 * 2 * 2
                 ...               1024
    */

    /* disregard current element */
    std::exclusive_scan(begin(v),
                        end(v),
                        std::ostream_iterator<int>(std::cout, " "),
                        1,                                              // neutral for multiplication
                        std::multiplies<int>());                        // binary predicate

    /* {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
        1                                                               // neutral instead of current
           2                                                            // 1 * 2 (previous element)
              4                                                         // 2 * 2 (previous elements)
                 8                                                      // 2 * 2 * 2 (previous elements)
                   ...             515
    */

    return 0;
}
