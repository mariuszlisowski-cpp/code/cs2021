#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6 };

    /* shifts the elements in the range by n positions
       from c++20 */
    std::shift_left(begin(v), end(v), 3);                   // 4, 5, 6, 0, 0, 0
    print_container(v);
    std::shift_right(begin(v), end(v), 2);                  // 0, 0, 4, 5, 6, 0
    print_container(v);

    return 0;
}
