#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void print(T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 8, 2, 5, 3, 4, 4, 2, 7, 6, 6, 1, 8, 9 };
    std::vector<int> x;
    std::vector<int> y;

    /* find all equal to 2, 4, 6 or 8 */
    std::copy_if(begin(v), end(v),
                 std::back_inserter(x),
                 [](auto e) {
                     return e == 2 or e == 4 or e == 6 or e == 8;
                 });
    print(x);
    
    /* find all greater than six */
    std::copy_if(begin(v), end(v),
                 std::back_inserter(y),
                 [](auto e) {
                     return e > 6;
                 });
    print(y);

    return 0;
}
