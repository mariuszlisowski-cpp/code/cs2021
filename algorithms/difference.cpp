#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(begin(container), end(container),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> common_part;
    
    std::vector<int> v1{ 1, 2, 3, 4, 5 }; 
    std::vector<int> v2{       3, 4, 5, 6, 7 }; 
    //                 { 1, 2 };                                // difference (v1 - v2)
    //                 {                6, 7};                  // difference (v2 - v1)

    std::set_difference(v1.begin(), v1.end(),                   // v1 - v2
                        v2.begin(), v2.end(),
                        std::back_inserter(common_part));
    print(common_part);

    std::set_difference(v2.begin(), v2.end(),                   // v2 - v1
                        v1.begin(), v1.end(),
                        common_part.begin());                   // caution: overwrites
    print(common_part);

    return 0;
}
