#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{ 8, 2, 5, 3, 4, 4, 2, 7, 6, 6, 1, 8, 9, 0 };

    auto biggerThanFive = 
        std::count_if(begin(v), end(v),
                    [](auto el) {
                        return el >= 5;
                    });
    std::cout << biggerThanFive << std::endl;

    return 0;
}
