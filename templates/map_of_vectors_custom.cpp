#include <algorithm>
#include <ios>
#include <iostream>
#include <iterator>
#include <optional>
#include <stdexcept>
#include <type_traits>
#include <vector>

template<typename Key, typename Value>
class VectorMap {
public:
    static_assert(std::is_default_constructible<Value>::value,
                  "An object Value cannot be instantinated!");

    void insert(const Key& key, const Value& value);
    Value& operator[](const Key& key);
    const Value at(const Key& key) const;

    auto size() const;
    bool isIntKey() const;                                              // check if Key is integral

private:
    std::vector<Key> keys;
    std::vector<Value> values;
};

template<typename Key, typename Value>
auto VectorMap<Key, Value>::size() const {
    return keys.size();
}

template<typename Key, typename Value>
void VectorMap<Key, Value>::insert(const Key& key, const Value& value) {
    std::cout << "insert: inserting '" << value 
              << "' at key " << key << std::endl;
    keys.push_back(key);
    values.push_back(value);
}

template<typename Key, typename Value>
bool VectorMap<Key, Value>::isIntKey() const {
    return std::is_integral<Key>();
}

template<typename Key, typename Value>
Value& VectorMap<Key, Value>::operator[](const Key& key) {
    auto it = std::find(keys.begin(), keys.end(), key);
    if (it != keys.end()) {
        auto index = std::distance(keys.begin(), it);
        std::cout << "[]: key at " << key 
                  << " found, returning " << values[index] << " | ";
        return values[index];
    }
    std::cout << "pushing key " << key << " with empty value | ";
    keys.push_back(key);
    values.push_back(Value{});                                          // value wiil be replaced anyway
    return values.back();
}

template<typename Key, typename Value>
const Value VectorMap<Key, Value>::at(const Key& key) const {
    auto it = std::find(keys.begin(), keys.end(), key);
    if (it != keys.end()) {
        auto index = std::distance(keys.begin(), it);
        std::cout << "at: key at " << key 
                  << " found, returning " << values[index] << " | ";
        return values[index];
    }
    throw std::out_of_range("No such key found!");
}


int main() {
    VectorMap<int, char> map;
    map.insert(1, 'a');                                                 // inserting at key 1
    map[1] = 'z';                                                       // finds key at 1, returns ref, replaces with z
    std::cout << map[1] << std::endl;                                   // finds key at 1, returns it (and prints)

    map[2] = 'b';                                                       // no key, creating with empty, replaces with b
    std::cout << map[2] << std::endl;                                   // finds key at 2, returns it (and prints)
    std::cout << "size: " << map.size() << std::endl;

    try {
        std::cout << map.at(2);                                         // throws std::out_of_range
    } catch (std::out_of_range& e) {
        std::cout << e.what() << std::endl;
    }

    std::cout << std::boolalpha << std::endl
              << map.isIntKey() << std::endl;

    return 0;
}
