#include <iostream>
#include <type_traits>

template<typename T, typename U>
auto addA(const T& lhs, const U& rhs) -> decltype(lhs + rhs) {          // c++11
    return lhs + rhs;
}

template<typename T, typename U>
auto addB(const T& lhs, const U& rhs) {                                 // c++14
    return lhs + rhs;
}

template<typename T, typename U>
std::common_type_t<T, U> addC(const T& lhs, const U& rhs) {             // c++11
    return lhs + rhs;
}

int main() {
    std::cout << addA(2, 3.14) << std::endl;
    std::cout << addB(2, 3.14) << std::endl;
    std::cout << addC(2, 3.14) << std::endl;

    return 0;
}
