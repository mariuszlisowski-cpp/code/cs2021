#include <iostream>
struct Bar {};

template<typename T>
void foo(const T& parameter) {
    std::cout << "templated function" << std::endl;
}

/*  full function specialization
    no partial function specialization is allowed in C++  */
template<>
void foo<Bar>(const Bar& parameter) {                                   // specialization for Bar object
    std::cout << "specialized function" << std::endl;
}

int main() {
    foo<int>(42);                                                       // templated function used

    Bar bar;
    foo(bar);                                                           // specialization for Bar object used

    return 0;
}
