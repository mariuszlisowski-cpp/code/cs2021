#include <ios>
#include <iostream>

/* primary template */
template<typename T>
struct is_int {
    static constexpr bool value = false;
};

/* explicit specialization for T = int */
template<>
struct is_int<int> {
    static constexpr bool value = true;
};

/* variable template
   used as helper of primary template */
template<typename T>
constexpr bool is_int_v = is_int<T>::value;

int main() {
    std::cout << std::boolalpha << is_int_v<char> << '\n';  // prints 0 (false)
    std::cout << std::boolalpha << is_int_v<int> << '\n';   // prints 1 (true)

    return 0;
}
