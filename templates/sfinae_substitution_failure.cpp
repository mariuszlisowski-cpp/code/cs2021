#include <iostream>
#include <typeinfo>

/* SFINAE (Substitution Failure Is Not An Error) can happen here */
template<typename T>
void foo(T* x) {
    std::cout << "foo<" << typeid(T).name() << ">(T*)\n";
}

void foo(int x) {
    std::cout << "foo(int)\n";
}

void foo(double x) {
    std::cout << "foo(double)\n";
}

int main() {
    foo(42);                                                            // template substitution failure
                                                                        // overload for 'int' wins
    foo(42.0);                                                          // saa (not error)

    foo("template wins here");

    return 0;
}
