#include <iostream>

template<typename T, typename U>
class Foo {
public:
    static_assert(std::is_default_constructible<U>::value,
                  "The second object lacks a default constructor");
};

struct Non_default_constructible {
    Non_default_constructible() = delete;
};

int main() {
    Foo<int, char> foo_pass;
    // Foo<int, Non_default_constructible> foo_fail;                       // cannot be instantiated

    return 0;
}
