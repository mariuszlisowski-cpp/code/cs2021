#include <iostream>

/* #1: overload for all types */
template<class T>
void f(T);                                                              // checked as 1st (as overload)

/* #1a: specialization of #1 */
template<>
void f(int*);                                                           // specialization NOT checked

/* #2: overload for all pointer types */
template<class T>
void f(T*);                                                             // checked as 2nd (as overload)

int main() {
    f(new int{1});                                                      // 1st overload rejected
                                                                        // 2nd accepted

    return 0;
}
