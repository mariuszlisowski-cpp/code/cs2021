/* auto p = new Data(10) means:
   - allocate sizeof(Data) bajtów on heap
   - call c'tor of Data
   - assign address to poiner p

   The order of evaluation of operands of almost all C++ operators(including the order
   of evaluationof function arguments in a function-call expression and the order of evaluation
   of the subexpressions within any expression) is unspecified.
*/
#include <memory>

struct Data {
    int value;
};

void sink(std::shared_ptr<Data> oldData,                            // by value
          std::shared_ptr<Data> newData)                            // saa
{
    // ...
}

/* no leaks */
int main() {
    auto oldD = std::shared_ptr<Data>{new Data{41}};                // object created (may throw an exception)
    auto newD = std::shared_ptr<Data>{new Data{42}};                // object created (if no exception thrown)
    sink(std::move(oldD), std::move(newD));                         // operands already initialized
                                                                    // both objects sank thus do not USE them
    return 0;
}
