#include <algorithm>
#include <iostream>

struct Point {
    int x;
    int y;
    void print() {
        std::cout << x << ' ' << y << std::endl;
    }
};

void processPointer(Point* md) {
    md->print();
}

void processElement(Point md) {
    std::cout << md.x << ' ' << md.y << std::endl;
}

int main() {
    const size_t size{ 10 };
    std::unique_ptr<Point[]> tab{ new Point[size] };                // creates a pointer to an array

    processPointer(tab.get());                                      // first el
    processPointer(&tab[0]);                                        // first el (using operator[])
    processPointer(&tab[1]);                                        // second el
    processElement(tab[size - 1]);                                  // last el

    return 0;
}                                                                   // calls delete[] for an array
