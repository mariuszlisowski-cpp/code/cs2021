#include <memory>

struct Data {};

/* kopiowanie wymaga inkrementacji / dekrementacji liczników
   musi być bezpieczne wielowątkowo
     std::atomic / std::lock nie są darmowe
   zawoła destruktory  */
namespace not_recommended {                                     // by value
void foo(std::shared_ptr<Data> p);
void bar(std::shared_ptr<Data> p) {
    foo(p);
}
}

/* tak szybkie jak przekazywanie zwykłego wskaźnika
   bez dodatkowych operacji
   może być niebezpieczne w aplikacjach wielowątkowych
     (jak każde przekazywanie przez &)
   */
namespace recommended {                                         // by ref to const smart
void foo(const std::shared_ptr<Data>& p);
void bar(const std::shared_ptr<Data>& p) {
    foo(p);
}
}

int main() {
    auto sptr = std::make_shared<Data>();

    not_recommended::bar(sptr);                                 // expensive

    return 0;
}
