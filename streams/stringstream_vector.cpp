#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

int main() {
    std::string s{ "Hello my beautiful world! How are you?" };

    std::istringstream iss(s);                                                  // convert to string stream
                                                                                // delimiter is a whitespace
    std::vector<std::string> v{ std::istream_iterator<std::string>(iss), {} };  // splitting to words

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<std::string>(std::cout, " "));

    return 0;
}
