#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <string>

namespace std {
std::ostream& operator<<(std::ostream& os, const std::pair<std::string, std::string>& p) {
    return os << '\'' << p.first << "' means '" << p.second << "' in english";
}
}

int main() {
    std::string polish_filename{ "polish.txt" };
    std::string english_filename{ "english.txt" };

    std::ifstream polish(polish_filename);
    std::ifstream english(english_filename);

    std::map<std::string, std::string> dict;
    std::transform(std::istream_iterator<std::string>(polish), {},
                   std::istream_iterator<std::string>(english),
                   std::inserter(dict, dict.begin()),
                   [](const std::string& polish_word,
                      const std::string& english_word) -> decltype(dict)::value_type
                   {
                       return { polish_word, english_word };
                   });

    std::copy(begin(dict), end(dict),
              std::ostream_iterator<decltype(dict)::value_type>(std::cout, "\n"));

    return 0;
}
