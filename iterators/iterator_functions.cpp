#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector v{ 1, 2, 3, 4, 5 };                 // uses random access iterator

    /* for input iterators */
    auto it = v.end();
    std::advance(it, -2);                           // increments by n elements
    /* error
    std::advance(std::end(v), -2);
    std::advance(v.end()), -2); */

    /* for bidirectional iterators */
    auto it_next = std::next(v.begin());            // default difference is 1
    auto it_prev = std::prev(v.end());              // default difference is 1

    it_next = std::next(v.begin(), 3);              // set difference
    it_prev = std::prev(v.end(), 3);                // set difference

    /* for input iterators but
       more efficient for radnom access iterator */
    std::distance(v.begin(), v.end());

    return 0;
}
