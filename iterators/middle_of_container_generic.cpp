#include <array>
#include <forward_list>
#include <iostream>
#include <list>
#include <queue>
#include <vector>

template <typename Iter>
auto middle(Iter first, Iter last) {
    auto size = std::distance(first, last);
    std::advance(first, size / 2);

    return *first;
}

int main() {
    std::vector v{ 1, 2, 3, 4, 5 };                                     // contiguous iterator
    std::array a{ 1, 2, 3, 4, 5 };                                      // contiguous iterator
    std::list l{ 1, 2, 3, 4, 5 };                                       // bidirectional iterator
    std::forward_list fl{ 1, 2, 3, 4, 5 };                              // forward iterator
    std::deque dq({ 1, 2, 3, 4, 5 });

    std::cout << middle(std::begin(v), std::end(v)) << std::endl;
    std::cout << middle(std::begin(a), std::end(a)) << std::endl;
    std::cout << middle(std::begin(l), std::end(l)) << std::endl;
    std::cout << middle(std::begin(fl), std::end(fl)) << std::endl;
    std::cout << middle(std::begin(dq), std::end(dq)) << std::endl;

    return 0;
}
