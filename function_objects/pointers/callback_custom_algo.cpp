#include <iostream>

int compute(int a, int b, auto operation) {             // deducted type: int (*operation)(int, int)
    return operation(a, b);
}

int sum(int a, int b) {
    return a + b;
}

int multiply(int a, int b) {
    return a * b;
}

int main() {
    auto result = compute(5, 6, sum);                   // pointer to funtion passed
    std::cout << result << std::endl;

    result = compute(5, 6, multiply);
    std::cout << result << std::endl;

    return 0;
}
