#include <iostream>

void function() {
    std::cout << "> function called" << std::endl;
}

int triple(int value) {
    return 3 * value;
}

int main() {
    function;                                   // pointer (not used)
    auto f = function;                          // pointer assigned to variable (alias)
                                                // type decucted: void (*f)()
    f();

    auto t = triple;                            // pointer assigned to variable (alias)
                                                // type decucted: int (*t)(int)

    auto tripled = triple(2);                   // value returned from function
    std::cout << tripled << std::endl;

    return 0;
}
