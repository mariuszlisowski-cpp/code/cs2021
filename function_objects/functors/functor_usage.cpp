#include <algorithm>
#include <iostream>
#include <vector>

struct Foo {
    void operator()(int el) {
        std::cout << el << ' ';
    }
};

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    std::for_each(begin(v), end(v), Foo{});                 // create temporary object many times

    Foo foo;
    std::for_each(begin(v), end(v), foo);                   // call existing object many times

    return 0;
}
