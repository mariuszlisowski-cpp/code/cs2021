#include <algorithm>
#include <iostream>
#include <vector>

struct Foo {
    void operator()(int el) {                                       // object is a functor now
        std::cout << index << ": " << el << " | ";                  // as it may be called by () operator
        ++index;
    }

    int index;
};

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    /* functor */
    std::for_each(begin(v), end(v), Foo{});
    std::cout << std::endl;

    std::for_each(begin(v), end(v),
                  [index = 0](int el) mutable {                     // lambda instead of functor
                      std::cout << index << ": " << el << " | ";    // index deducted during init
                      ++index;
                  });

    return 0;
}
