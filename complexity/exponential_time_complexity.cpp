/* exponential time complexity 

   O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
-> O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

// fibo:     0 1 1 2 3 5 8 13 21 34 55 89 144 ...
// index:    0 1 2 3 4 5 6  7  8  9 10 11  12
int fibonacci(int n) {
    if (n <= 1) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

int main() {
    /* recurstion is exponental time*/

    /* recursive fibonacci O(2^n) */
    std::cout << fibonacci(8) << std::endl;
    std::cout << fibonacci(40) << std::endl;

    /* examples:
       recursive deep-first search in graphs */

    return 0;
}
