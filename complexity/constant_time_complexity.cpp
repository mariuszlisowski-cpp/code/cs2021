/* constant time complexity 
   
-> O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
long arithmetic_sequence_sum(const std::vector<T>& v) {
    if (v.empty()) {
        return 0;
    }
    //  mathematical formula for arithmetic seq sum
    return (v.front() + v.back()) / 2 * v.size();                               // O(1) complexity
                                                                                // as independent of the size
}

int main() {
    std::vector<int> v(4);                                                      // size doesn't matter...
    std::generate(begin(v), end(v),
                  [i{ -1 }]() mutable { return i += 2; });

    std::copy(begin(v), end(v),
              std::ostream_iterator<int>(std::cout, " "));

    std::cout << '\n' << arithmetic_sequence_sum(v);                            // for the sum calculation

    return 0;
}
