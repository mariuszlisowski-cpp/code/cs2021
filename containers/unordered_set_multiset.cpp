#include <iostream>
#include <unordered_set>

int main() {
    /* set */
    std::unordered_set<int> us;

    us.begin();                                                 
    // us.rbegin();                                                 // forward iterator only
    
    auto pair = us.insert(1);
    if (pair.second) {
        std::cout << "success " << *pair.first << std::endl;
    }
    pair = us.emplace(1);                                           // already exists
    if (!pair.second) {
        std::cout << "failure " << std::endl;
    }

    auto size = us.erase(1);                                        // how many erased (0 or 1 as uniques)
    std::cout << size << std::endl;
    size = us.erase(9);                                             // none erased
    std::cout << size << std::endl;

    us.rehash(10);                                                  // no of buckets
    auto hash = us.hash_function();
    std::cout << hash(1) << std::endl;


    /* multiset */
    std::unordered_multiset<int> ums{ 3, 3, 3, 3, 4, 5, 6, 6 };
    
    auto range = ums.equal_range(3);                                // find how many keys
    std::cout << std::distance(range.first, range.second)           // no. of found keys
              << std::endl;
    for (; range.first != range.second; ++range.first) {
        std::cout << *range.first << ' ';
    }
    std::cout << std::endl;

    if (range.first != ums.end() &&
        range.second != ums.end()) {
        std::cout << *range.first << " - "
                  << *range.second << std::endl;                    // nullptr (!)
    }

    size = ums.erase(3);                                            // four threes erased      
    std::cout << size << std::endl;
    std::cout << ums.load_factor() << std::endl;

    return 0;
}
