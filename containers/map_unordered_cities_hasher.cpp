#include <algorithm>
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <string>

struct Point {
    int x;
    int y;
    bool operator==(const Point& other) const {                             // in case of collision
        return x == other.x && y == other.y;
    }
};

class HashFucntion {
public:
    size_t operator()(const Point& other) const {                           // callable object
        size_t h1 = std::hash<int>{}(other.x);
        size_t h2 = std::hash<int>{}(other.y);
        return h1 ^ (h2 << 1);                                              // hashing function definition
    }
};

using Cities = std::unordered_map<Point, std::string, HashFucntion>;        // callable object as a hash function

double radius(const Point& p) {
    return std::sqrt(p.x * p.x + p.y * p.y);                                // Pythagoras theorem
}

/* 1. */
void print_within_diameter(const Cities& cities, double diameter) {
    auto isCloserThan = [diameter](const auto& coords) {
        return radius(coords) < diameter;
    };
    for (const auto& [coords, city] : cities) {
        if (isCloserThan(coords)) {
            std::cout << '(' << coords.x << ", " 
                    << coords.y << ") " 
                    << city << " radius: "
                    << radius(coords) << std::endl;            
        }
    }
}

/* 2. */
void print_city_coords(const Cities& cities, const std::string& city_to_find) {
    for (const auto& [coords, city] : cities) {
        if (city == city_to_find) {
            std::cout << city <<  " (" << coords.x << ", "
                    << coords.y << ')' << std::endl;
        }                                                                       // else ?
    }
}

int main() {
    Cities cities{
        { { 151, -33 }, "Sydney" },
        { { 17, 51 }, "Wroclaw" },
        { { 37, 55 }, "Moskwa" },
        { { -74, 40 }, "Nowy Jork" },
    };

    /* print container */
    std::for_each(begin(cities), end(cities), [](auto& pair) {
        std::cout << pair.second << ' ';
    });
    std::cout << std::endl;

    /* 1. within diameter from (0, 0) */
    constexpr size_t DIAMETER{ 70 };
    print_within_diameter(cities, DIAMETER);

    /* 2. city coordinates */
    print_city_coords(cities, "Sydney");

    /* other methods */
    for (const auto& [coord, city] : cities) {
        std::cout << "bucket index: " << cities.bucket(coord) 
                  << " of " << city << std::endl;
    }

    std::cout << "bucket count: " << cities.bucket_count() << std::endl;
    cities.rehash(10);                                                      // new bucket count (can be higher)
    std::cout << "bucket count: " << cities.bucket_count() << std::endl;

    return 0;
}
