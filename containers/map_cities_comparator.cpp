// implemented using custom comparator for a 'Point' class
// no need for comparator in a 'Point' class

#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <string>

struct Point {
    int x;
    int y;
};

auto comp = [](const auto& lhs, const auto& rhs) {
    return radius(lhs) < radius(rhs);                                           // custom comparator
};

using Cities = std::map<Point, std::string, decltype(comp)>;                    // use of custom comparator

double radius(const Point& p) {
    return std::sqrt(p.x * p.x + p.y * p.y);                                    // Pythagoras theorem
}

void print_within_diameter(const Cities& cities, double diameter) {
    auto isCloserThan = [diameter](const auto& coords) {
        return radius(coords) < diameter;
    };
    for (const auto& [coords, city] : cities) {
        if (isCloserThan(coords)) {
            std::cout << '(' << coords.x << ", " 
                    << coords.y << ") " 
                    << city << " radius: "
                    << radius(coords) << std::endl;            
        }
    }
}

void print_city_coords(const Cities& cities, const std::string& city_to_find) { 
   for (const auto& [coords, city] : cities) {
        if (city == city_to_find) {
            std::cout << city <<  " (" << coords.x << ", "
                      << coords.y << ')' << std::endl;
        }                                                                       // else ?
    }
}

int main() {
    /* comparator is global due to using Cities alias */
    Cities cities{                                                              // decltype compulsory
        {
            { { 151, -33 }, "Sydney" },
            { { 17, 51 }, "Wroclaw" },
            { { 37, 55 }, "Moskwa" },
            { { -74, 40 }, "Nowy Jork" }
         }, comp                                                                // comparator
    };

    /* alternatively
    std::map<Point, std::string, decltype(comp)> cities(comp);
    cities.insert({ { 151, -33 }, "Sydney" }); ... */

    /* print container */
    std::for_each(begin(cities), end(cities), [](auto& pair) {
        std::cout << pair.second << ' ';
    });
    std::cout << std::endl;

    /* within diameter from (0, 0) */
    constexpr size_t DIAMETER{ 70 };
    print_within_diameter(cities, DIAMETER);

    /* city coordinates */
    print_city_coords(cities, "Sydney");

    return 0;
}
