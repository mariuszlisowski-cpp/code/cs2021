#include <iostream>
#include <map>

template<typename T>
void print(T& container) {
    for (const auto& [key, value] : container) {
        std::cout << key << value << std::endl;
    }
}

int main() {
    std::multimap<char, int> mm;
    mm.insert({ 'a', 1 });
    mm.insert({ 'a', 1 });                                      // inserted
    mm.insert({ 'a', 1 });                                      // saa
    print(mm);

    // mm['b'] = 2;                                            // no access operator[] nor 'at'

    std::map<char, int> m;
    m.insert({ 'b', 2 });
    m.insert({ 'b', 2 });                                       // no effect as key exists
    m.insert({ 'b', 4 });                                       // saa (in spite of value)
    print(m);

    std::cout << m['b'];                                        // output: 2
    m['c'] = 3;                                                 // insert new
    std::cout << m['c'];                                        // output: 3
    m['b'] = 1;                                                 // replace existing
    std::cout << m['b'];                                        // output: 1

    return 0;
}
