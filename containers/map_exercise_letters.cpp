#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

template<typename T>
void print_three_letters_long(std::multimap<T, std::string> mm) {
    std::for_each(begin(mm), end(mm), [](const auto& pair) {
        if (pair.second.size() == 3) {
            std::cout << pair.second << std::endl;
        }
    });
}

template<typename T>
void print_three_letters_long_trans(std::multimap<T, std::string> mm) {
    std::transform(begin(mm), end(mm),
                   std::ostream_iterator<std::string>(std::cout, " "),
                   [](const auto& pair) -> std::string {
                       return pair.second.size() == 3 ? pair.second : "";
                   });
}

int main() {
    std::multimap<int, std::string> map;
    map.insert({5, "Ala"});
    map.insert({5, "Ma"});
    map.insert({5, "Kota"});
    map.insert({5, "A"});
    map.insert({5, "Kot"});
    map.insert({5, "Ma"});
    map.insert({5, "Ale"});

    print_three_letters_long(map);

    return 0;
}
