/* Zadanie:
     1. Wygeneruj zbiór liczb od -20 do 20 i zapisz go w std::set w kolejności malejącej
     2. Wstaw nowe elementy: -10, 0, 10, 100, -100
     3. Wygeneruj drugi zbiór z liczb od 0 do 40 i zapisz go w std::multiset
     4. Wstaw nowe elementy: -10, 0, 10, 100, -100
     5. Połącz oba zbiory w jeden (ma to być std::multiset)
     6. Znajdź wszystkie elementy równe 0 i 50
   Co każdy krok wypisuj zawartości kontenerów. */

// #include <algorithm>
#include <iostream>
#include <numeric>
#include <set>
#include <vector>

template<typename T>
void print(const T& container) {
    for (auto el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    /* 1 */
    std::vector<int> v(41);                                                 // [-20, 20]
    std::iota(v.begin(), v.end(), -20);
    print(v);

    std::set<int, std::greater<int>> s(v.begin(), v.end());
    print(s);

    /* 2 */
    s.insert(-10);                                                          // no effect as duplicated
    s.insert(0);                                                            // saa
    s.insert(10);                                                           // saa
    s.insert(100);                                                          // ok
    s.insert(-100);                                                         // ok
    print(s);

    /* 3 */
    std::iota(v.begin(), v.end(), 0);
    std::multiset<int, std::less<int>> ms(v.begin(), v.end());              // std::less redundants
    print(ms);

    /* 4 */
    ms.insert(-10);                                                          // ok
    ms.insert(0);                                                            // ok
    ms.insert(10);                                                           // ok
    ms.insert(100);                                                          // ok
    ms.insert(-100);                                                         // ok
    print(ms);
    
    /* 5 */
    ms.merge(s);
    print(ms);
    
    /* 6 */
    auto [lower, upper] = ms.equal_range(0);
    for (; lower != upper; ++lower) {                                       // not equal
        std::cout << *lower << ' ';                                         // elements found
    }

    auto pair = ms.equal_range(50);
    lower = pair.first;
    upper = pair.second;
    for (; lower != upper; ++lower) {                                       // equal
        std::cout << *lower << ' ';                                         // no elements found
    }

    return 0;
}
