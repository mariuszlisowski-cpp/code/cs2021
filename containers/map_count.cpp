#include <iostream>
#include <map>

template<typename T>
void print(T& container) {
    for (const auto& [key, value] : container) {
        std::cout << key << value << std::endl;
    }
}

int main() {
    std::map<char, int> m;

    auto it = m.begin();
    m.insert(std::pair<char, int>('a', 1));                     // inserts pair
    m.insert({ 'b', 2 });                                       // inserts pair (initializer_list)
    m.insert_or_assign(it, 'c', 2);                             // inserts pair
    m['d'] = 1;                                                 // inserts pair

    /* for std::map returns 1 or 0 (exists or not) */
    std::cout << "count: " << m.count('a') << std::endl;        // one
    std::cout << "count: " << m.count('b') << std::endl;
    std::cout << "count: " << m.count('c') << std::endl;
    std::cout << "count: " << m.count('d') << std::endl;

    std::cout << "count: " << m.count('e') << std::endl;       // zero

    /* for std::multimap returns same key count */
    std::multimap<char, int> mm;
    mm.insert({ 'a', 1 });                                      // first inserted
    mm.insert({ 'a', 2 });                                      // second inserted
    mm.insert({ 'a', 3 });                                      // third inserted
    print(mm);

    std::cout << "count: " << mm.count('a') << std::endl;       // three

    return 0;
}
