#include <algorithm>
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <string>

struct  Point {
    int x;
    int y;
};

using Cities = std::unordered_map<std::string, Point>;                  // string already has a hash function

double radius(const Point& p) {
    return std::sqrt(p.x * p.x + p.y * p.y);                            // Pythagoras theorem
}

void print_within_diameter_algo(const Cities& cities, double diameter) {
    auto it = cities.begin();
    while (it != cities.end()) {
        it = std::find_if(it, end(cities),
                          [diameter](const auto& pair) {
                              return radius(pair.second) < diameter;
                          });
        std::cout << '(' << it->second.x << ", " 
                  << it->second.y << ") " 
                  << it->first << std::endl;
        ++it;
    }
}

void print_within_diameter(const Cities& cities, double diameter) {
    auto isCloserThan = [diameter](const auto& coords) {
        return radius(coords) < diameter;
    };

    for (const auto& [city, coords] : cities) {
        if (isCloserThan(coords)) {
            std::cout << '(' << coords.x << ", " 
                    << coords.y << ") " 
                    << city << " radius: "
                    << radius(coords) << std::endl;
        }
    }
}

void print_city_coords(const Cities& cities, const std::string& city) { 
    auto it = cities.find(city);
    if (it != cities.end()) {
        auto& [city, coord] = *it;                                      // structrural binding
        std::cout << city << " (" << coord.x << ", "
                << coord.y << ')' << std::endl;
    }
}

int main() {
    std::unordered_map<std::string, Point> cities{
        {"Wroclaw", { 17, 51 }},
        {"Moskwa", { 37, 55 }},
        {"Nowy Jork", { -74, 40 }},
        {"Sydney", { 151, -33 }}
    };

    /* within diameter from (0, 0) */
    constexpr double DIAMETER{ 70 };
    print_within_diameter(cities, DIAMETER);
    // print_within_diameter_algo(cities, DIAMETER);

    /* city coordinates */
    print_city_coords(cities, "Sydney");

    /* additional methods */    
    std::cout << cities.bucket_count() << std::endl;                    // how many buckets in acontainer
    std::cout << cities.bucket_size(0) << std::endl;                    // which bucket (index)
    std::cout << cities.load_factor() << std::endl;                     // average no. of elements per bucket
    std::cout << cities.max_load_factor() << std::endl;
    std::cout << cities.bucket("Sydney") << std::endl;                  // bucket index

    return 0;
}
