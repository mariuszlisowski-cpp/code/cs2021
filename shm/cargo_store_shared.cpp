#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Cargo {
public:
    Cargo(std::string name) : name_(name) {}
    std::string name_;
};

class Store {
public:
    Store() {
        // 1.
        // std::shared_ptr<Cargo> banana(new Cargo("banana"));

        // 2.
        // std::shared_ptr<Cargo> banana;
        // banana = std::make_shared<Cargo>("banana");
        // cargo_.push_back(banana);

        // 3.
        cargo_.push_back(std::make_shared<Cargo>("banana"));
        cargo_.push_back(std::make_shared<Cargo>("lemon"));
        cargo_.push_back(std::make_shared<Cargo>("kiwi"));
    }

    void printCargo() {
        std::cout << "> in a store: ";
        for (auto& cargo : cargo_) {
            std::cout << cargo->name_ << ' ';
        }
        std::cout << std::endl;
     }

    void setName(std::string name) {
        if (!cargo_.empty()) {
            cargo_[0]->name_ = name;
        }
    }

    std::vector<std::shared_ptr<Cargo>> cargo_;
};

class Ship {
public:
    Ship(Store* store) : store_(store) {}

     void printCargo() {
         std::cout << "> on a ship: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ';
         }
         std::cout << std::endl;
     }

    void copyCargo() {
        for (const auto& cargo: store_->cargo_) {
            cargo_.push_back(cargo);
        }
    }

    std::vector<std::shared_ptr<Cargo>> cargo_;
    Store* store_;
};

int main() {
    Store store;
    Ship ship(&store);

    store.printCargo();
    ship.copyCargo();
    ship.printCargo();
    
    store.setName("potato");
    ship.printCargo();
}
