#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Cargo {
public:
    Cargo(std::string name, int amount) : name_(name), amount_(amount) {}
    std::string name_;
    int amount_;
};

class Store {
public:
    Store() {
        cargo_.push_back(std::make_unique<Cargo>("banana", 10));
        cargo_.push_back(std::make_unique<Cargo>("lemon", 20));
        cargo_.push_back(std::make_unique<Cargo>("kiwi", 30));
    }

    void printCargo() {
         std::cout << "> in a store: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ' << cargo->amount_ << ", ";
         }
         std::cout << std::endl;
     }

    void setAmount(const std::string& name, int amount) {
        auto cargo_ship_it{ std::find_if(begin(cargo_), end(cargo_),
                                   [&name](const auto& cargo){
                                       return cargo->name_ == name;
                                   })
                     };

        if (cargo_ship_it != cargo_.end()) {
            (*cargo_ship_it)->amount_ = amount;
        }
    }

    std::vector<std::unique_ptr<Cargo>> cargo_;
};

class Ship {
public:
    Ship(Store* store) : store_(store) {}

    void printCargo() {
        std::cout << "> on a ship: ";
        for (auto& cargo : cargo_) {
            std::cout << cargo->name_ << ' ' << cargo->amount_ << ", ";
        }
        std::cout << std::endl;
    }

    auto findCargoOnShip(const std::string& name) {
        return std::find_if(begin(cargo_), end(cargo_),
                            [&name](const auto& cargo){
                                return cargo->name_ == name;
                            });
    }

    auto findCargoInStore(const std::string& name) {
        return std::find_if(begin(store_->cargo_), end(store_->cargo_),
                            [&name](const auto& cargo){
                                return cargo->name_ == name;
                            });
    }

    void buyCargo(const std::string& name, int amount) {
        auto cargo_ship_it{ findCargoOnShip(name) };
        auto cargo_store_it{ findCargoInStore(name) };
        if (cargo_store_it != store_->cargo_.end()) {                                   // cargo present in store
            if (amount >= (*cargo_store_it)->amount_) {                                 // buy all cargo from store
                std::cout << "# buy all" << std::endl;
                if (cargo_ship_it != cargo_.end()) {                                    // cargo present on ship
                    std::cout << "# cargo present on ship" << std::endl;
                    (*cargo_ship_it)->amount_ += (*cargo_store_it)->amount_;
                } else {                                                                // no such cargo on ship
                    std::cout << "# no such cargo on ship yet" << std::endl;
                    cargo_.push_back(std::move(*cargo_store_it));                       // transfer all cargo to ship
                }
                store_->cargo_.erase(cargo_store_it);                                   // delete cargo from store
            } else {                                                                    // buy a few cargo from store
                std::cout << "# buy a few" << std::endl;
                if (cargo_ship_it != cargo_.end()) {                                    // cargo present on ship
                    std::cout << "# cargo present on ship" << std::endl;
                    (*cargo_ship_it)->amount_ += amount;                                // increase amount on ship
                } else {
                    std::cout << "# no such cargo on ship yet" << std::endl;
                    cargo_.push_back(std::make_unique<Cargo>(name, amount));            // create new cargo on ship
                }
                (*cargo_store_it)->amount_ -= amount;                                   // decrease amount in store
            }
        } else {                                                                        // no such cargo in store                   
            std::cout << "# no such cargo in store!" << std::endl;
        }
    }

    void sellCargo(const std::string& name, int amount) {
        auto cargo_ship_it{ findCargoOnShip(name) };
        auto cargo_store_it{ findCargoInStore(name) };
        if (cargo_ship_it != cargo_.end()) {                                            // cargo present on ship
            if (amount >= (*cargo_ship_it)->amount_) {                                  // sell all cargo from ship
                std::cout << "# sell all" << std::endl;
                if (cargo_store_it != store_->cargo_.end()) {                           // cargo present in store
                    std::cout << "# cargo present in store" << std::endl;
                    (*cargo_store_it)->amount_ += (*cargo_ship_it)->amount_;
                } else {                                                                // no such cargo in store
                    std::cout << "# no such cargo in store yet" << std::endl;
                    store_->cargo_.push_back(std::move(*cargo_ship_it));                // transfer all cargo to store
                }
                cargo_.erase(cargo_ship_it);                                            // delete cargo from ship
            } else {                                                                    // sell a few cargo from ship
                std::cout << "# sell a few" << std::endl;
                if (cargo_store_it != store_->cargo_.end()) {                           // cargo present in store
                    std::cout << "# cargo present in store" << std::endl;
                    (*cargo_store_it)->amount_ += amount;
                } else {                                                                // no such cargo in store
                    std::cout << "# no such cargo in store yet" << std::endl;
                    store_->cargo_.push_back(std::make_unique<Cargo>(name, amount));    // create new cargo in store
                }
                (*cargo_ship_it)->amount_ -= amount;                                    // decrease amount on ship
            }
        } else {                                                                        // no such cargo on ship
            std::cout << "# no such cargo on ship!" << std::endl;
        }
    }

    void setAmount(const std::string& name, int amount) {
        auto cargo_ship_it{ std::find_if(begin(cargo_), end(cargo_),
                                   [&name](const auto& cargo){
                                       return cargo->name_ == name;
                                   })
                     };

        if (cargo_ship_it != cargo_.end()) {
            (*cargo_ship_it)->amount_ = amount;
        }
    }

    std::vector<std::unique_ptr<Cargo>> cargo_;
    Store* store_;
};

int main() {
    Store store;
    Ship ship(&store);

    store.printCargo();

    ship.buyCargo("banana", 99);                            // buy all banana (10 bought)
    store.printCargo();
    ship.printCargo();

    ship.buyCargo("banana", 6);                             // no such cargo in store
    ship.sellCargo("lemon", 2);                             // no such cargo on ship

    ship.buyCargo("lemon", 12);                             // ok
    store.printCargo();
    ship.printCargo();
    
    ship.sellCargo("lemon", 99);                            // sell all cargo (12 sold)
    store.printCargo();
    ship.printCargo();

    store.setAmount("banana", 11);                          // no effect (empty vector)
    ship.setAmount("banana", 99);                           // ok
    ship.printCargo();
}
