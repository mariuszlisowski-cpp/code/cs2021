#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Cargo {
public:
    Cargo(std::string name, int amount) : name_(name), amount_(amount) {}
    std::string name_;
    int amount_;
};

class Store {
public:
    Store() {
        cargo_.push_back(std::make_unique<Cargo>("banana", 1));
        cargo_.push_back(std::make_unique<Cargo>("lemon", 2));
        cargo_.push_back(std::make_unique<Cargo>("kiwi", 3));
    }

    void printCargo() {
         std::cout << "> in a store: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ' << cargo->amount_ << ", ";
         }
         std::cout << std::endl;
     }

    void setAmount(const std::string& name, int amount) {
        auto cargo_it{ std::find_if(begin(cargo_), end(cargo_),
                                   [&name](auto& cargo){
                                       return cargo->name_ == name;
                                   })
                     };

        if (cargo_it != cargo_.end()) {
            (*cargo_it)->amount_ = amount;
        }
    }

    std::vector<std::unique_ptr<Cargo>> cargo_;
};

class Ship {
public:
    Ship(Store* store) : store_(store) {}

     void printCargo() {
         std::cout << "> on a ship: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ' << cargo->amount_ << ", ";
         }
         std::cout << std::endl;
     }

    void moveCargo() {
        for (auto& cargo: store_->cargo_) {
            cargo_.push_back(std::move(cargo));
        }
        store_->cargo_.clear();                         // vector cleared
    }

    void setAmount(const std::string& name, int amount) {
        auto cargo_it{ std::find_if(begin(cargo_), end(cargo_),
                                   [&name](auto& cargo){
                                       return cargo->name_ == name;
                                   })
                     };

        if (cargo_it != cargo_.end()) {
            (*cargo_it)->amount_ = amount;
        }
    }

    std::vector<std::unique_ptr<Cargo>> cargo_;
    Store* store_;
};

int main() {
    Store store;
    Ship ship(&store);

    store.printCargo();
    ship.moveCargo();
    store.printCargo();                                 // dangling pointers in a vector if not cleared
    ship.printCargo();
    
    store.setAmount("banana", 11);                      // no effect (empty vector)
    ship.setAmount("banana", 11);
    ship.printCargo();
}
