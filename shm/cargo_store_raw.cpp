#include <iostream>
#include <string>
#include <vector>

class Cargo {
public:
    Cargo(std::string name) : name_(name) {}
    std::string name_;
};

class Store {
public:
    Store() {
        Cargo* banana{ new Cargo("banana") };
        cargo_.push_back(banana);
        Cargo* lemon{ new Cargo("lemon") };
        cargo_.push_back(lemon);
        Cargo* kiwi{ new Cargo("kiwi") };
        cargo_.push_back(kiwi);
    }
    ~Store() {
        for (auto& cargo : cargo_) {
            delete cargo;
        }
        cargo_.clear();
    }

    void printCargo() {
         std::cout << "> in a store: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ';
         }
         std::cout << std::endl;
     }

    void setName(std::string name) {
        if (!cargo_.empty()) {
            cargo_[0]->name_ = name;
        }
    }

    std::vector<Cargo*> cargo_;
};

class Ship {
public:
    Ship(Store* store) : store_(store) {}

     void printCargo() {
         std::cout << "> on a ship: ";
         for (auto& cargo : cargo_) {
             std::cout << cargo->name_ << ' ';
         }
         std::cout << std::endl;
     }

    void copyCargo() {
        for (const auto& cargo: store_->cargo_) {
            cargo_.push_back(cargo);
        }
    }

    std::vector<Cargo*> cargo_;
    Store* store_;
};

int main() {
    Store store;
    Ship ship(&store);

    store.printCargo();
    ship.copyCargo();
    ship.printCargo();
    
    store.setName("potato");
    ship.printCargo();
}
