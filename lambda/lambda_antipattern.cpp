#include <iostream>

int main() {
    /* antipattern */
    constexpr auto add = [](int n, int m) {             // outer lambda
        auto L = [=] { return n; };                     // 1st inner lambda
        auto R = [=] { return m; };                     // 2nd inner lambda
        return [=] { return L() + R(); };               // returned lambda
    };
    
    static_assert(add(3, 4)() == 7);                    // use returned lambda

    return 0;
}
