#include <iostream>
#include <memory>

int main() {
    /* expression instead capture*/
    auto lambdaA = [value = 1] { return value; };                       // type auto (deduced)
    auto lambdaB = [value{0}]() mutable { return value++; };            // type auto (explicit value a MUST)

    /*  */
    std::unique_ptr<int> ptr(new int(10));
    {
    auto anotherLambda = [value = std::move(ptr)] { return *value; };   // lambda is the owner of ptr
    std::cout << anotherLambda() << std::endl;
    }                                                                   // unique ptr goes out of scope with lambda

    return 0;
}
