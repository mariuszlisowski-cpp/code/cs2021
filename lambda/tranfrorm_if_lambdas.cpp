#include <iostream>
#include <numeric>
#include <vector>

template<typename PRED>
auto filter(PRED predicate) {
    return [=](auto map_func) {                                             // capture predicate
        return [=](auto init, auto arg) {                                   // capture map_func
            if (predicate(arg)) {                                           // if predicate...
                return map_func(init, arg);                                 // thus tranform
            }
            return init;                                                    // do nothing
        };
    };
}

/* reduction */
template<typename FUN>
auto map(FUN trans) {
    return [=](auto reduce) {                                               // reduce function
        return [=](auto init, auto arg) {
            return reduce(init, trans(arg));
        };
    };
}

template<typename IN, typename OUT, typename PRED, typename FUN>
OUT transform_if(IN first, IN last, OUT out, PRED pred, FUN trans) {
    auto copy_and_increment = [](auto out, auto arg) {
        *out = arg;
        return ++out;
    };
    return std::accumulate(first, last,
                           out,                                             // output iterator
                           filter(pred) (map(trans) (copy_and_increment) )
                           );
}

int main() {
    std::vector<int> v{ 2, 5, 7, 10, 12, 16, 21 };
    std::vector<int> out;

    transform_if(v.begin(), v.end(),
                 std::back_inserter(out),
                 [](auto num) { return num % 2 == 0; },
                 [](auto num) { return num * 2; });

    for (auto el : out) {
        std::cout << el << ' ';
    }

    return 0;
}
