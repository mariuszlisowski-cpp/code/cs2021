#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

struct Lambda {
    Lambda(const char& letter) : letter(letter) {}                  // initialization via c'tor
    char operator()() {
        if (letter > 'z') {
            letter = 'A';
        } else if (letter > 'Z' && letter < 'a') {
            letter = 'a';
        }

        return letter++;
    }

    char letter;
};

int main() {
    /* same as struct solution */
    auto lambda = [letter{ 'a' }]() mutable {                       // initialization via c'tor
        if (letter > 'z') {
            letter = 'A';
        } else if (letter > 'Z' && letter < 'a') {
            letter = 'a';
        }

        return letter++;
    };

    auto l = Lambda('a');                                           // struct initialization

    /* test */
    for (size_t i = 1; i < (26 * 4) + 1; i++) {                     // four times alphabet
        std::cout << l();                                           // functor called (struct object)
        std::cout << lambda();                                      // lambda called
        if (i % 26 == 0) {                                          // divide each alphabet
            std::cout << std::endl;
        }
    }

    return 0;
}
