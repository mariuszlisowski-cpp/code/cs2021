/* Lambda as an example of functional programming

Characteristics of functional programming:
- first-class functions
- higher-order functions
- immutable data
- pure functions
- recursion
- manipulation of lists
- lazy evaluation

An object is first-class when it:
- can be stored in variables and data structures
- can be passed as a parameter to a subroutine
- can be returned as the result of a subroutine
- can be constructed at runtime
- has intrinsic identity (independent of any given name)

LAMBDA -> closure class
closure -> object of closure class
closure -> any function that closes over the environment in which it was defined
        -> it can access variables not in its parameter list
 */
#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>

/* f(x,y) = x^2 + y^2 + 2xy */
auto fxysquare = [](int x, int y) ->int {
    int xsquare = [x]() { return x * x; }();
    int ysquare = [y]() { return y * y; }();
    int twoxy = [x, y]() { return 2 * x * y;}();
    return xsquare + ysquare + twoxy;
};

std::function<void()> foo() {
    int number{100};                                                                // captured number
    return [=]() {
        std::cout << "Function returning lambda whith a captured number: " 
                  << number << '\n';
    };
}

int main() {
    /* lambda preserving a local variable */
    auto bar = foo();
    bar();                                                                          // remebered number

    /* lambda in lambda */
    auto timesThreePlusFive = [](int number) {
        return [=]() {
            return number * 3;
        }() + 5;
    };
    std::cout << "Numbler * Three + Five: " 
              << timesThreePlusFive(4) << '\n';                                     // 4 * 3 + 5

    /* lambda returns lambda */
    auto multiplyByNumber = [](int value) -> std::function<int(int)> {
        return [=](int number) { return value * number; };
    };                                                                              // cannot be used with <<

    /* lambda takes lambda as an argument */
    int number{4};
    auto addTen = [number](const std::function<int(int)>& lambda) {
        return lambda(number) + 10;
    };
    auto result = addTen(multiplyByNumber(5));                                      // 5 * 4 + 10
    std::cout << "Number * Five + Ten: " << result << '\n';

    /* quadratic equation */
    std::cout << "quadratic equation: " << fxysquare(5, 6) << '\n';

    /* persist captured value */
    int k = 6;
    auto lambda = [k](int number) { return k + number; };                           // variable captured & saved
    std::cout << "same: " << lambda(3) << '\n';
    k++;                                                                            // incremented but...
    std::cout << "same: " << lambda(3) << '\n';                                     // save value of variable
    k += 5;
    std::cout << "same: " << lambda(3) << '\n';                                     // saa

    return 0;
}
