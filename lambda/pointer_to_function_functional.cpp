/* pointer to the function via functional examples:
    std::function<int()> f                  - f takes no arguments and returns and int
    std::function<void(int)> f              - f takes an int and returns nothing
    std::function<double(int, string)> f    - f takes an int and a string and returns double
    
    funtinal vs raw:
    std::function<double(int, string)> f
                  double(*f)(int, string)
                        ^^^^                // removed
 */
#include <iostream>
#include <functional>

/* accepts a pointer to the function
   signature within < > */
template<typename T>
auto foo(std::function<T()> function_ptr) {                 // instead of 'T(*function_ptr)()' as a parameter
    return function_ptr();                                  // returns T type
}

int main() {
    auto lambdaA = [] { return 42; };
    std::cout<< foo<int>(lambdaA);

    auto lambdaB = []() { return true; };
    std::cout<< std::boolalpha
             << foo<bool>(lambdaB);

    int value{};
    auto lambdaC = [value] { return 42; };                  // captured value
    std::cout<< foo<int>(lambdaC);                          // converted  '<lambda()>' to type 'int (*)()'

    return 0;
}
