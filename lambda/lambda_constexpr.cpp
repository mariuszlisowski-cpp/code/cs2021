#include <array>
#include <iostream>

int main() {
    /* all lambdas, if possible, are implicitly marded as constexpr
       from c++17 */
    auto squaredA = [](auto x) {                                        // implicitly marked as constexpr
        return x * x;
    };

    auto squaredB = [](auto x) constexpr {                              // explicitly marded
        return x * x;
    };

    std::array<int, squaredA(3)> arr;
    std::cout << arr.size() << std::endl;

    return 0;
}
