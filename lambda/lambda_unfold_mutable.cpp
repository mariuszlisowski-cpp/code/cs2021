#include <iostream>

int main() {
    double number = 0.5;
    [number]() mutable { number += 1.0; }();                        // operator() NOT const

    /* unfolded lambda */
    class __lambda_6_3 {
    public: 
        __lambda_6_3(double & _number)
        : number{_number} {}

        inline void operator()() {                                  // operator NOT const
            number += 1.0;
        }
        
    private: 
        double number;                                              // value
        
        public:
        
    } __lambda_6_3{number};

    __lambda_6_3.operator()();                                      // call unfolded lambda

    std::cout << number << std::endl;                               // no change (as passed by value)

    return 0;
}
