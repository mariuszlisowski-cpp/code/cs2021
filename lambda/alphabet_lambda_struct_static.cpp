#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

struct Lambda {                                                     // NO c'tor (!)
    char operator()() {
        if (letter > 'z') {
            letter = 'A';
        } else if (letter > 'Z' && letter < 'a') {
            letter = 'a';
        }

        return letter++;
    }

    static char letter;                                             // static member (non const)
};                                                                  // can be initialized only outside struct

char Lambda::letter = 'a';                                          // struct initialization

int main() {
    /* similar to struct solutino */
    auto lambda = [letter{ 'a' }]() mutable {                       // initialization via c'tor (!)
        if (letter > 'z') {
            letter = 'A';
        } else if (letter > 'Z' && letter < 'a') {
            letter = 'a';
        }

        return letter++;
    };

    /* test */
    for (size_t i = 1; i < (26 * 4) + 1; i++) {                     // four times alphabet
        std::cout << Lambda()();                                    // functor called (struct object)
        std::cout << lambda();                                      // lambda called
        if (i % 26 == 0) {                                          // divide each alphabet
            std::cout << std::endl;
        }
    }
    

    return 0;
}
