#include <iostream>

/* solution with by value capture */
auto getIndexGenerator() {
    int value = 0;
    auto lambda = [value]() mutable {                       // capture by value
        return value++;
    };

    return lambda;                                          // returned initialized with zero
}

/* solution with static capture */
auto getIndexGeneratorStatic() {
    static int value = 0;
    auto lambda = [] {                                      // capture static by value
        return value++;
    };

    return lambda;                                          // returned initialized with zero
}

int main() {
    auto generatorA = getIndexGenerator();                  // labmda returned (value initialized with zero)
    auto generatorC = getIndexGenerator();                  // labmda returned (value initialized with zero)
    
    /* saa */
    auto generatorB = [value = 0]() mutable {               // initialized with zero
        return value++;
    };

    for (int i = 0; i < 10; ++i) {
        std::cout << generatorA();
        std::cout << generatorB();
        std::cout << generatorC();
    }
    
    return 0;
}
