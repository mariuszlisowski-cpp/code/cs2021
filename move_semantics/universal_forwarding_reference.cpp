#include <iostream>
#include <utility>

/* ref collapsing:
   - single & on left or right gives l-value
      & &  -> &
      & && -> &
      && & -> &
   - double && on left and right give r-value
      && && -> &&
*/

struct Bar {};

void foo(const Bar& bar) {
    std::cout << "l-value reference" << std::endl;
}

void foo(Bar&& bar) {
    std::cout << "r-value reference" << std::endl;
}

template<typename T>
void universal_reference(T&& bar) {                                     // called also 'forwarding reference'
    foo(std::forward<T>(bar));
}

int main() {
    Bar bar;
    universal_reference(bar);                                           // l-value called (ref & && -> &)

    universal_reference(Bar{});                                         // r-value called (value -> &&)

    return 0;
}
