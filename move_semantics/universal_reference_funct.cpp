#include <iostream>

template <typename T>
void copy(T arg) {
    std::cout << "copy" << std::endl;
}

template <typename T>
void reference(T& arg) {
    std::cout << "reference" << std::endl;
}

template <typename T>
void universal_reference(T&& arg) {
    std::cout << "universal reference" << std::endl;
}

int main() {
    auto number{42};
    
    copy(number);                                       // int  (l-value)
    copy(24);                                           // int  (r-value)
    reference(number);                                  // int& (l-value)
    
    // reference(24);                                   // candidate function [with T = int] not viable:
                                                        // expects an l-value for 1st argument
    
    universal_reference(number);                        // int&  (l-value)
    universal_reference(std::move(number));             // int&& (r-value now)
    universal_reference(24);                            // int&& (r-value)

    return 0;
}
