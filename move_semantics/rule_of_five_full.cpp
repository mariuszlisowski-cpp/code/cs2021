#include <ostream>
#include <iostream>
#include <string>
#include <vector>

struct Coords
{
    double latitude;
    double longitude;
};

class City
{
public:
    City(const std::string& name, const Coords& coords)
        : name(name)
        , coords(coords) {}

    friend std::ostream& operator<<(std::ostream& os, const City& city);

private:
    std::string name;
    Coords coords;
};

std::ostream& operator<<(std::ostream& os, const City& city) {
    os << "City:\t\t" << city.name << '\n'
        << "Latitude:\t" << city.coords.latitude << '\n'
        << "Longitude:\t" << city.coords.longitude << '\n';
    return os;
}

template<typename T>
class Pointer
{
public:
    explicit Pointer(T obj) {
        ptr = new T(obj); 
    }
    Pointer(const Pointer& other) : Pointer(*other.ptr) {               // reuse of default c'tor (preferred)
        std::cout << "> copy constructor"  << std::endl;                // deep copy mandatory
        // ptr = new T(*other.ptr);                                     // could be used instead of initializer list
    }
    Pointer(Pointer&& other) {                                          // no initializer list used
        std::cout << "> move constructor"  << std::endl;
        ptr = other.ptr;                                                // could be done in initializer list
        other.ptr = nullptr;                                            // d'tor calls delete on nullptr
    }
    Pointer& operator=(const Pointer& other) {
        std::cout << "> copy assignment"  << std::endl;
        if (this != &other) {
            *ptr = *other.ptr;                                          // reuse of object's memory
        }                                                               // thus no delete called
        return *this;
    }
    Pointer& operator=(Pointer&& other) {
        std::cout << "> move assignment"  << std::endl;
        if (this != &other) {
            // delete ptr;
            // ptr = other.ptr;
            // other.ptr = nullptr;                                     // or instead ...
            std::swap(*this, other);                                    // safe to delete after swapping
        }
        return *this;
    }
    ~Pointer() {
        std::cout << "> destructor"  << std::endl;
        delete ptr;
    }

    T get() {
        return *ptr;
    }

private:
    T* ptr;
};

int main() {
    Pointer<City> cityA(City("Sosnowiec", {50.286263, 19.104078}));
    Pointer<City> cityB(City("Katowice", {50.270908, 19.039993}));

    Pointer<City> cityC(cityA);                                         // copy c'tor
    std::cout << cityC.get() << std::endl;
    
    cityC = cityB;                                                      // copy assignment
    std::cout << cityC.get() << std::endl;

    cityC = std::move(cityA);                                           // move assignment
    std::cout << cityC.get() << std::endl;

    Pointer<City> cityD(std::move(cityC));                              // move c'tor
    std::cout << cityD.get() << std::endl;
}
