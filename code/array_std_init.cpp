#include <array>
#include <string>
#include <iostream>
 
int main() {
    /* construction uses aggregate initialization */
    std::array<int, 3> a1{ {1, 2, 3} };                         // double-braces required in C++11 prior to
                                                                // the CWG 1270 revision (not needed in C++11
                                                                // after the revision and in C++14 and beyond)
 
    std::array<int, 3> a2 = {1, 2, 3};                          // double braces never required after =
 
    std::array<std::string, 2> a3 = { std::string("a"), "b" };

    /* deduction guide for array creation (since C++17) */
    std::array a4{3.0, 1.0, 4.0};                               // -> std::array<double, 3>

    return 0;
}
