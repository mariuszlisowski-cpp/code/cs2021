#include <iostream>

int main() {
    int value{ 9 };
    const int* ptrA{ &value };                          // pointer to const value
    int const* ptrB{ &value };                          // pointer to const value
    // *ptrA = 1;                                       // read-only
    // *ptrB = 1;                                       // read-only

    int* const ptrC{ &value };                          // const pointer
    *ptrC = 1;
    // ptrC = nullptr;                                  // read only

    const int* const ptrD{ &value };                    // const pointer to const value
    // *ptrD = 1;                                       // read only
    // ptrD = nullptr;                                  // read only

    return 0;
}
