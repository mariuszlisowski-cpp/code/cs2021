#include <memory>
#include <iostream>

std::shared_ptr<int> calculateProduct(int a, int b) {
    auto result{ std::make_shared<int>(a * b) };
    std::cout << "> owners inside: " << result.use_count() << std::endl;

    return result;
}

int main() {
    auto result{ calculateProduct(4, 5) };
    
    std::cout << *result << std::endl;
    std::cout << "> owners outside: " << result.use_count() << std::endl;

    return 0;
}
