/* 
resize(): This lets you change the size of the vector to any size you want.
    It will fill the underlying buffer with elements.
reserve(): This changes the capacity of the vector. Note that this doesn’t change the vector’s size, it just changes the size of the underlying buffer, to give more room for expansion of the buffer before the buffer has to be resized. Unlike calling resize(), this doesn’t change the behavior of the program, just the performance (Subsequent use of the reserved space will not incur a performance penalty for incremental reservations).
    It will not limit the size of the buffer. If the buffer runs out of space it will automatically reallocate as needed.
*/

#include <iostream>
#include <vector>

int main() {
    int cap{};
    std::vector<int> v;
    
    std::cout << "> cap increase: ";
    for (size_t i = 0; i < 50; ++i) {
        if (cap != v.capacity()) {
            std::cout << v.capacity() << ' ';
        }
        cap = v.capacity();
        v.push_back(i);
    }

    v.reserve(128);                                             // wasting memory
    std::cout << "\n\n> size: " << v.size() << std::endl;
    std::cout << "> cap:  " << v.capacity() << std::endl;
    
    // v.reserve(128);                                          // redundant
    v.resize(128);
    std::cout << "\n> size: " << v.size() << std::endl;
    std::cout << "> cap:  " << v.capacity() << std::endl;

    v.reserve(256);
    v.shrink_to_fit();                                          // invalidate reserve
    std::cout << "\n> size: " << v.size() << std::endl;
    std::cout << "> cap:  " << v.capacity() << std::endl;

    return 0;
}
