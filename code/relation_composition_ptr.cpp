/* Composition - OWNS relationship 
   indirect containment - by pointer
*/
#include <iostream>

class Room {
public:
    void livingroom() {
        std::cout << "> lama in the livingroom\n";
    };
};

class House {
public:
    House() {
        bedroom = new Room;
    }
    ~House() {
        delete bedroom;
    }
    Room* get_bedroom() {
        return bedroom;
    }
private:
    Room* bedroom;                          // pointer containment
};                                          // house OWNS the room and it is
                                            // destroyed when the house is
int main() {
    {
        House house;
        house.get_bedroom()->livingroom();
    }                                       // house and room destruction

    return 0;
}
