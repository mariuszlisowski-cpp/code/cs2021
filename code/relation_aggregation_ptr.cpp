/* Aggregation - HAS relationship
   implemented using a pointer
*/
#include <iostream>

class Architect {
public:
    void design() {
        std::cout << "> house designed\n";
    }
};

class House {
public:
    Architect* architect;
};

int main() {
    Architect architect;
    {
        House house;
        house.architect->design();
    }                                   // house destruction
    architect.design();                 // but architect still exists

    return 0;
}
