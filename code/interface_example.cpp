#include <string>

/* abstract class */
class Bird {
public:
    size_t getWeight() const    { return weight_; }
    size_t getHeight() const    { return height_; }
    std::string getName() const { return name_; }

    // pure virtual methods (without implementation)s
    virtual void eat() = 0;                             // at least one virtual method
    virtual void sleep() = 0;                           // can be stored in a pointer

protected:
    size_t weight_;
    size_t height_;
    std::string name_;
};

/* interfaces */
class Flyable {
public:
    virtual void fly() = 0;                             // only virtual method(s)
};

class Swimmable {
public:
    virtual void swim() = 0;
};

class Soundable {
public:
    virtual void makeSound() = 0;
};

/* using interfaces */
class Penguin : public Bird,
                public Swimmable,
                public Soundable
{
public:
    void eat() override;                                // from Bird
    void sleep() override;

    void swim() override;                               // from Swimmable

    void makeSound() override;                          // from Soundable
};

class Hummingbird : public Bird,
                    public Flyable
{
public:
    void eat() override;                                // from Bird
    void sleep() override;

    void fly() override;                                // from Flyable
};

/* using interfaces */
class Goose : public Bird,
              public Flyable,
              public Soundable,
              public Swimmable
{
public:
    void eat() override;                                // from Bird
    void sleep() override;

    void makeSound() override;                          // from Soundable

    void fly() override;                                // from Flyable

    void swim() override;                               // from Swimmable
};
