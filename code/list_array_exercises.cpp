#include <array>
#include <iostream>
#include <list>

template<typename T>
void print(const T& l) {
    for (auto el : l ) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

template<typename T, size_t N>
void print_array(const std::array<T, N>& arr) {
    for (auto& el : arr ) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::list<int> l{ 0, 1, -1, 2, 3, 4 };
    print(l);

    l.erase(std::next(l.begin(), 2));                       // erases an iterator (or range)
    print(l);

    l.push_front(-99);
    l.push_back(-99);
    print(l);

    l.insert(std::next(l.begin(), 3), -77);                 // inserts a value using iterator
    print(l);

    l.remove(-99);                                          // removes a value
    print(l);
    
    std::array<int, 6> arr;
    auto it{ l.begin() };
    for (size_t i{}; i < l.size(); ++i) {
        arr[i] = *it;
        std::advance(it, 1);
    }
    print_array(arr);

    return 0;
}
