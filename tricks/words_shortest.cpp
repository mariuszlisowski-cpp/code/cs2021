#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>
#include <map>
#include <string>

namespace std {
    std::ostream& operator<<(std::ostream& os, const std::pair<int, std::string>& pair) {
        os << pair.second << " : " << pair.first << " occurences" << std::endl;
        return os;
    }
}

void remove_punctuation(std::string& str) {
    str.erase(std::find_if(begin(str), end(str), ::ispunct),
              str.end());
}

int main() {
    std::ifstream file("words_lengths_multimap.cpp");
    if (!file) {
        return 1;
    }
    
    /* count lengths */
    std::multimap<int, std::string> word_lengths;
    std::transform(std::istream_iterator<std::string>(file), {},
                   std::inserter(word_lengths, begin(word_lengths)),
                   [](std::string str) -> decltype(word_lengths)::value_type {         // look at definition of
                                                                                       // decltype (typedefs)
                       remove_punctuation(str);
                       return { str.length(), std::move(str) };
                   });

    /* shortst words 
       read range from the beginning */
    const auto shortest = word_lengths.equal_range(word_lengths.cbegin()->first);      // pair of iterators
    const auto [low, high] = word_lengths.equal_range(word_lengths.cbegin()->first);   // iterators (binding)

    /* print 
       variations */
    std::copy(shortest.first, shortest.second,                                          // low / high of range
              std::ostream_iterator<std::pair<int, std::string>>(std::cout));           // no deduction
    std::copy(low, high,
              std::ostream_iterator<std::pair<int, std::string>>(std::cout));           // saa

    std::copy(low, high,
              std::ostream_iterator<decltype(word_lengths)::value_type>(std::cout));    // deducec value type

    return 0;
}
