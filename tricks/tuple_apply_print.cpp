#include <initializer_list>
#include <iostream>
#include <string>
#include <tuple>

struct Foo {
    Foo(std::string&& str) : str(std::move(str)) {}
    friend std::ostream& operator<<(std::ostream& os, const Foo& foo) {
        os << foo.str;
        return os;
    }

    std::string str;
};

int main() {
    auto print_tuple = [](const auto... args) {
        (void)std::initializer_list<int>{((void)(std::cout << args << ' '), 0)...}; // expression not used (void)
    };                                                                              // side effect of 'cout' only

    std::tuple<std::string, int, Foo> tuple{"string", 42, Foo{"foo"}};

    std::apply(print_tuple, tuple);

    return 0;
}
