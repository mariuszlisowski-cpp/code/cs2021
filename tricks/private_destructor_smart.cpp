/*  Whenever we want to control destruction of objects of a class,
    we make the destructor private. For dynamically created objects,
    it may happen that you pass a pointer to the object to a function
    and the function deletes the object. If the object is referred
    after the function call, the reference will become dangling.
 */
#include <functional>
#include <iostream>
#include <memory>

class Foo {
public:
    void print() {
        std::cout << "> I'm Foo" << std::endl;
    }

private:
    ~Foo() {}

    friend void delete_resource(Foo* ptr);                              // may be both private or public
};

void delete_resource(Foo* ptr) {
    // additional deletetion code
    std::cout << "# deleting resource" << std::endl;
    delete ptr;
}

int main() {
    /* shared pointer */
    {
        auto shared = std::shared_ptr<Foo>(new Foo, delete_resource);   // custom deleter
        shared->print();
    }                                                                   // deleting resource


    /* unique pointer */
    auto unique = std::unique_ptr<Foo, std::function<void(Foo*)>>(new Foo, delete_resource);
    unique->print();

    return 0;
}                                                                       // deleting resource
