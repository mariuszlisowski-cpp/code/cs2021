#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>

struct Point {
    int x;
    int y;
    bool operator==(const Point& other) const {                             // in case of collision
        return x == other.x && y == other.y;
    }
};

struct HashFucntion {
        size_t operator()(const Point& other) const {                           // callable object
        size_t h1 = std::hash<int>{}(other.x);
        size_t h2 = std::hash<int>{}(other.y);
        return h1 ^ (h2 << 1);                                              // hashing function definition
    }
};

using Cities = std::unordered_map<Point, std::string, HashFucntion>;        // callable object as a hash function

int main() {
    Cities cities{
        { { 151, -33 }, "Sydney" },
        { { 17, 51 }, "Wroclaw" },
        { { 37, 55 }, "Moskwa" },
        { { -74, 40 }, "Nowy Jork" },
    };

    /* print container */
    std::for_each(begin(cities), end(cities),
                  [](auto& pair) {
                      std::cout << pair.second << std::endl;
                  });

    return 0;
}
