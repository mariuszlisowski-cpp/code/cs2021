#include <iostream>
#include <memory>
#include <vector>

int& foo() {                                        // returns a reference
    static int s = 5;                               // memory location: initialized data (never deleted)
    std::cout << s << std::endl;

    return s;
}

static int& foo() {}                                // saa (but making more confusing)

int main() {
    ++foo();                                        // increment reference
    ++foo();                                        // increment reference
    foo();                                          // not incremented
    foo();                                          // not incremented
    
    return 0;
}
