#include <iostream>
#include <string>
#include <type_traits>

template<typename TAG, typename T>
class IP {
    std::string ip;
};

int main() {
    IP<class IPv4, std::string> ipv4;                                  // cannot use 'typename'
    IP<class IPv6, std::string> ipv6;                                  // saa

    return 0;
}
