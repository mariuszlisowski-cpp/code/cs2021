#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>

void rewind_file(std::ifstream& stream) {
    if (stream.fail()) {
        stream.clear();
    }
    stream.seekg(0, stream.beg);                                        // offset zero from 'beg'
}

int main() {
    std::string filename{"rewind_file_multiple_read.cpp"};
    std::ifstream file(filename);

    std::copy(std::istreambuf_iterator<char>(file),                     // first it
              {},                                                       // last it
              std::ostreambuf_iterator<char>(std::cout));

    rewind_file(file);
    std::cout << std::string(60, '#') << std::endl;

    std::copy(std::istreambuf_iterator<char>(file),
              {},
              std::ostreambuf_iterator<char>(std::cout));

    return 0;
}
