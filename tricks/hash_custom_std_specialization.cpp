#include <iomanip>                                                                  // quoted
#include <iostream>
#include <string>
#include <unordered_set>

struct Person {
    std::string first_name;
    std::string last_name;
};

/* in case of collision */
bool operator==(const Person& lhs, const Person& rhs) {
    return lhs.first_name == rhs.first_name &&
           lhs.last_name == rhs.last_name;
}
 
/* custom hash can be a standalone function object */
struct MyHash {
    std::size_t operator()(Person const& s) const noexcept {
        std::size_t h1 = std::hash<std::string>{}(s.first_name);
        std::size_t h2 = std::hash<std::string>{}(s.last_name);
        return h1 ^ (h2 << 1);                                                      // combine hashes
    }                                                                               // or use boost::hash_combine
};
 
/* custom specialization of std::hash can be injected in namespace std */
namespace std {
    template<> struct hash<Person> {
        std::size_t operator()(Person const& s) const noexcept {
            std::size_t h1 = std::hash<std::string>{}(s.first_name);
            std::size_t h2 = std::hash<std::string>{}(s.last_name);
            return h1 ^ (h2 << 1);                                                  // combine hashes
        }                                                                           // or use boost::hash_combine
    };
}

int main() {
    /* already defined 'string' specialization */
    std::string str = "Meet the new boss...";
    std::size_t str_hash = std::hash<std::string>{}(str);
    std::cout << std::quoted(str) << ": " << str_hash << '\n';

    /* custom hash function */
    Person obj = { "Alice", "Smith" };
    std::cout << std::quoted(obj.first_name) << ", "
              << std::quoted(obj.last_name) << ": "
              << MyHash{}(obj) << " (using MyHash)\n" << std::setw(18)              // using the standalone function
              << "or " << std::hash<Person>{}(obj)                                  // or custom specialization in std::
              << " (using injected std::hash<Person> specialization)\n";
 
    /* custom hash makes it possible to use custom types in unordered containers
       The example will use the injected std::hash<Person> specialization above */
    std::unordered_set<Person> personsA {
        obj,                                                                        // Alice Smith
        {"Cindy", "White"},
        {"Tori", "Black"}
    };
    /* to use MyHash instead, pass it as a second template argument */
    std::unordered_set<Person, MyHash> personsB {
        {"Cindy", "White"},
        {"Tori", "Black"}
    };
    
    for(auto& s: personsA) {
        std::cout << s.first_name << ' ' 
                  << s.last_name << '\n';
    }

    return 0;
}
