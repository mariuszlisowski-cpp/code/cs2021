#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>
#include <map>
#include <string>

void remove_punctuation(std::string& str) {
    str.erase(std::find_if(begin(str), end(str), ::ispunct),
              str.end());
}

int main() {
    std::ifstream file("words_lengths_multimap.cpp");
    if (!file) {
        return 1;
    }
    
    /* count lengths */
    std::multimap<int, std::string> word_lengths;
    std::transform(std::istream_iterator<std::string>(file), {},
                   std::inserter(word_lengths, begin(word_lengths)),
                   [](std::string str) -> decltype(word_lengths)::value_type {   // look at definition of decltype
                       remove_punctuation(str);
                       return { str.length(), std::move(str) };
                   });

    /* print */
    for (const auto& [length, word] : word_lengths) {
        std::cout << length << " : " << word << std::endl;
    }

    return 0;
}
