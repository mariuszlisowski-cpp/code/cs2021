#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>

auto countWordsOccurences(const std::string& str) {
    std::map<std::string, int> mp;
    std::istringstream iss(str);                                            // input stream
    std::for_each(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  [&mp](const auto& word) {
                      mp[word]++;
                  });

    return mp;
}

int main() {
    std::string s{
        "ala ma kota\n"
        "a kot nie ma\n"
        "a kot to ala"
    };

    for (const auto& [word, times] : countWordsOccurences(s)) {
        std::cout << word << "\t :" << times << "\ttime(s)" << std::endl;
    }

    return 0;
}
