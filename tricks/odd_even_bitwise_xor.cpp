#include <iostream>

 /* XOR Operation of the Number by 1 increment the value of the number by 1 if the number is even
    otherwise it decrements the value of the number by 1 if the value is odd.
 */

int main() {
    int odd{ 1 };                                   // last bit set
    int even{ 2 };                                  // last bit NOT set

    if (even ^ 1 == even + 1) {
        std::cout << "> even" << std::endl;
    } else {
        std::cout << "> odd" << std::endl;
    }

    return 0;
}
