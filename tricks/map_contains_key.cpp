#include <iostream>
#include <map>
#include <string>

template<typename T, typename K>
bool contains(const std::map<T, K>& m, const T& key) {
    if (m.count(key)) {                                                                     // map count method
        return true;                                                                        // returns 0 or 1
    }

    return false;
}

int main() {
    std::map<std::string, float> m{
        { "one", 1.1 },
        { "two", 2.2 },
        { "three", 3.3 },
    };

    /* instead of c++20 'contains' method */
    std::cout << (contains(m, std::string("three")) ? "key exists" : "key does NOT exists") 
              << std::endl;
    std::cout << (contains(m, std::string("eitht")) ? "key exists" : "key does NOT exists") 
              << std::endl;

    return 0;
}
