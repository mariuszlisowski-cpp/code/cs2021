#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>

std::size_t countCharacters(const std::string& text) {
    std::istringstream ss(text);
    return std::distance(std::istreambuf_iterator<char>(ss), {});
                      // std::istreambuf_iterator<char>());                 // alternatively
}

std::size_t countWords(const std::string& text) {
    std::istringstream ss(text);
    return std::distance(std::istream_iterator<std::string>(ss), {});
}

std::size_t countLines(const std::string& text) {
    std::istringstream ss(text);
    return std::count(std::istreambuf_iterator<char>(ss), {}, '\n');
}

int main() {
    std::string s{              // 2 lines
        "ala ma kota\n"         // 11 chars + LF, 3 words
        "a kot nie ma\n"        // 12 chars + LF, 4 words
    };                          // 12       + 2,  7

    std::cout << "chars: " << countCharacters(s) << std::endl;
    std::cout << "words: " << countWords(s) << std::endl;
    std::cout << "lines: " << countLines(s) << std::endl;

    return 0;
}
