class Game {
public:
    Game() {}
    Game(int level) : level_(level) {}

private:
    int level_;
};

class Stage : public Game {
public:
    using Game::Game;                       // call base class constructors
};

int main() {
    Stage stageA;                           // using base class constructor
    Stage stageB(5);                        // saa
}
