#include <iostream>
#include <stdexcept>

void bar() try {
    /* tryging whole function body */
    throw std::runtime_error("trying the whole function");
    
} catch (std::exception& e) {
    std::cout << "exception: " << e.what() << std::endl;
    throw;                                                  // runtime_error rethrow
}

int main() try {
    /* trying whole main body */
    bar();

} catch (const std::runtime_error& le) {
    std::cout << "runtime: " << le.what() << std::endl;
}
