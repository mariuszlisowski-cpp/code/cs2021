#include <iostream>
#include <stdexcept>

struct TalkingObjectA {
    TalkingObjectA()  { std::cout << "A c'tor" << '\n'; }
    ~TalkingObjectA() { std::cout << "A d'tor" << '\n'; }
};

struct TalkingObjectB {
    TalkingObjectB()  { std::cout << "B d'tor" << '\n'; }
    ~TalkingObjectB() { std::cout << "B tor" << '\n'; }
};

void foo() { throw std::runtime_error("Error"); }

void bar() try {
    TalkingObjectB inside;
    foo();
} catch(std::exception const&) {
    std::cout << "exception" << '\n';
    throw;
}

int main() {
    TalkingObjectA outside;
    try {
        bar();
    } catch(std::runtime_error const& ex) {
        std::cout << "runtime_error: " << ex.what() << '\n';
    }
}